#python 

import os
import sys
from ffmpy import FFmpeg
import glob
import shutil
##### TBD : 
## get the creation date from the input file (first one) and apply it to the merged file.


### R U N 
 # python3.6 gopro_combine_files.py "/Volumes/NO NAME/DCIM/125GOPRO/" /Users/anton/Downloads/

if (sys.argv[1]):
	inputPath = sys.argv[1]
else:
	inputPath = ""

if (sys.argv[2]):
	outputPath = sys.argv[2]
else:
	outputPath = ""

firstFile = "GOPR????.MP4"

## single video or first video in chapter naming = GOPRxxxx.mp4 xxxx 0001-9999
## second and on video in chapter GPzzxxxx.mp4 zz=01-99, xxxx= 0001-9999

def combine():
	print(' Welcome to the merger ! ')
	file_list = getSinglesOrFirstVids()
	print(file_list)
	for file in file_list:
		sortedChapterFiles = []
		digits = getDigits(file) # the non changing element of the file naming
		print(" going through ditigs : " + str(digits))
		chapterFiles = glob.glob(inputPath + "/" + "GP??" + str(digits) + ".MP4" )

		# if the chapters are not found then it's a single file - copy it to the destination. 
		if len(chapterFiles)==0:
			print(" copy single file : ")
			filename = file.split("/")[-1]
			print(file +  " to " + outputPath.replace("\\","/") + filename)
			shutil.copy2(file, outputPath.replace("\\","/") + filename)

		print(" chapter files " + str(chapterFiles))
		sortedChapterFiles.append(file)
		for x in range(1,len(chapterFiles)+1):
			seq = "0" + str(x) if x<10 else str(x)
			sortedChapterFiles.append(inputPath + "GP" + seq + str(digits) + ".MP4")
		print(sortedChapterFiles)
		# import pdb
		# pdb.set_trace()
		if len(sortedChapterFiles) > 1:
			mergeFiles(sortedChapterFiles, digits)


def getMP4FileList():
	all_files = os.listdir(inputPath)
	print(" checking the path " + inputPath + " for files")
	mp4_files = []
	for file in all_files:
		if (".mp4" in file.lower()):
			mp4_files.append(file)
	print(" found files " + mp4_files)
	return mp4_files

def getSinglesOrFirstVids():
	print(" scanning the input path : " , inputPath)
	firstVideos = glob.glob(inputPath + "/" + firstFile )

	return firstVideos

def getDigits(filepath):
	filename = filepath.split("/")[-1]
	digits = filename[4:-4]
	return digits


def mergeFiles(fileChapter, digits):
	outname = outputPath.replace("\\","/") + "gopro_" + str(digits) + ".MP4"
	outname_dated = outputPath.replace("\\","/") + "/" + "gopro_" + str(digits) + "_date.MP4"
	# input file has to be stored locally to the script otherwise it will break. 
	# there is a bug with relative and absolute paths if input file is stored elsewhere.
	input_file = "input.txt"
	f = open(input_file,"w+")
	for file in fileChapter:
		f.write('file ' + file.replace('\\', '/').replace(' ', '\ ') + '\n')
	f.close()

	ff = FFmpeg(
		inputs = {input_file: ['-safe', '0','-loglevel', 'error','-f','concat']},
		outputs ={ outname : ['-c','copy', '-map', '0']}
		)
	ff.run()

	# ff2 = FFmpeg(
	# 	inputs = {outname: ['-loglevel', 'error']},
	# 	outputs ={outname_dated: ['-c','copy', '-map', '0', '-metadata', 'creation_time=2018-10-10T10:10:10' ]}
	# 	)
	# ff2.run()

if __name__ == "__main__":
	combine()