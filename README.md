GoPro video files merger
========================

##How to run 

python3.6 gopro_combine_files.py "/Volumes/GOPRO32/DCIM/125GOPRO/" /Users/anton/Downloads/

##Background

I got tired of goPro splitting my videos into files of 2gb because of filesystem limitation. Initially I uploaded all those files to google photos as they were, but it makes no sense to have multiple parts of the same video on google photos. Then I was using ffmpeg to manually merger the files using command line. Since I'm supposed to be a developer I decided to write a script to do it for me. 

The script will scan the input location for video files which are split into multiple parts (chapters) and then combine those chapters (of the same video) into one file. Additionally the script will (should) updated the creation_date metadata value of the merged file to the timestamp from the first chapter timestamp of the video. In that way it should be easier to search google photos for the right file.

It only works for gopro hero 4 because the naming convention of the files on gopro. It might be easily modified for other versions of gopro cameras. 
